$(document).ready(function(){
    $('.collapse-header').click(function(){
        $(this).toggleClass("active");
        // console.log($(this).data('collaps'));

        $($(this).data('collaps')).slideToggle( "slow", function() {
            // Animation complete.
          });
    })

    $('.bar').click(function(){
        $('.bar').toggleClass('open-c');
        $('.dash-menu').toggleClass('dash-menu-open')
    })

    
    $(".dash-menu-nav ul li:first").addClass("active");
    $(".menu-body-tap").hide();
    $(".menu-body-tap:first").show();
    $(".dash-menu-nav ul li").on("click", function () {
        $(".dash-menu-nav ul li").removeClass("active");
        $(this).addClass("active");
        $(".menu-body-tap").hide();
        $($(this).data("tap")).fadeIn();

    });
    $('.body-img ul li').click(function(){
        $('.body-img ul li').removeClass('active');
        $(this).addClass('active');
    })
});
